<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // ESTILOS PROPIOS CSS
        'css/site.css',

        // FUENTES AGREGADAS - GOOGLE FONTS
        'https://fonts.googleapis.com/css2?family=Cuprum:ital,wght@0,400;0,700;1,400&family=Oswald:wght@300;400;500;700&display=swap"',

        // LIBRERÍA ANIMATE AGREGADA
        'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css',
        
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
