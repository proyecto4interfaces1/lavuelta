<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\View;

class RutaController extends Controller
{
    public function actionIndex()
    {
        // Configura el título de la página
        $this->view->title = 'Rutas';

        // Configura las migas de pan (breadcrumbs)
        $this->view->params['breadcrumbs'][] = 'Rutas';

        // Resto de la acción
        return $this->render('index');
    }
}
