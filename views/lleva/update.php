<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\lleva $model */

$this->title = 'Update Lleva: ' . $model->numetapa;
$this->params['breadcrumbs'][] = ['label' => 'Llevas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numetapa, 'url' => ['view', 'numetapa' => $model->numetapa, 'código' => $model->código]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lleva-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
