<?php

use app\models\Ciclista;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ciclistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclista-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ciclista', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dorsal',
            'nombre',
            'edad',
            'nomequipo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ciclista $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dorsal' => $model->dorsal]);
                 }
            ],
        ],
    ]); ?>


</div>
