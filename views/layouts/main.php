<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/img/logo.ico')]); ?>
    <title>
        <?= Html::encode($this->title) ?>
    </title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header>
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-expand-md fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                [
                    'label' => 'Inicio',
                    'url' => ['/site/index'],
                    'options' => ['class' => ' menu-item animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Equipos',
                    'url' => ['/equipo/index'],
                    'options' => ['class' => ' menu-item animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Etapas',
                    'url' => ['/etapa/index'],
                    'options' => ['class' => ' menu-item animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Clasificaciones',
                    'url' => ['/lleva/index'],
                    'options' => ['class' => ' menu-item animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Curiosidades',
                    'url' => ['/site/contact'],
                    'options' => ['class' => ' menu-item animate__animated animate__fadeIn']
                ],
                /*['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
                )*/
            ],
        ]);
        NavBar::end();
        ?>
    </header>

    <main role="main" class="flex-shrink-0">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer class="footer mt-auto py-3 text-muted">
        <div class="container">
            <p class="float-left text-center">&copy; Ada Lovelace
                <?= date('Y') ?>
            </p>
        </div>
    </footer>

    <?php $this->endBody() ?>


    <script src="<?= Yii::$app->request->baseUrl ?>/js/animation.js" defer></script> <!-- Agrega esta línea -->
</body>

</html>
<?php $this->endPage() ?>